#include "timer.h"

void timer2_init(){
  LPC_SC->PCONP |= (1<<22); //Enable power Timer2
  //PCLK = CCLK/4
  //NO pin configuration needed
  LPC_TIM2->MCR |= 3; //Interrupt on match with MR0 and reset TC value
  NVIC->ISER[0] |= (1 <<3); //interrupt enable for Timer2
}

void timer2_start(uint32_t time){
  LPC_TIM2->MR0 = time * 24096; // 24096 == 1 ms
  LPC_TIM2->TCR |= 2; // reset the timer count registers
  LPC_TIM2->TCR &= ~(2);
  LPC_TIM2->TCR |= 1; // start timer2
}
