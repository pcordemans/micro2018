#ifndef UART_H
#define UART_H
#include "LPC17xx.h"
class UART{
public:
  /**
    * Initializes UART 0, baudrate 9600, 8N1
    * Preconditions: PCLK = 24MHz
    */
  UART();

  ~UART();
  /**
    * Blocking receive of a single character
    * @return a single character
    */
  char getc();

  /**
    *  Blocking transmit of a single character
    *  @param c character to transmit
    */
  void putc(char c);
};

#endif
