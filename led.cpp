#include "led.h"
#include "stdint.h"
#include "LPC17xx.h"

void led_init(uint8_t port, uint8_t pin){

  LPC_GPIO1->FIODIR |= 1 << pin;
  LPC_GPIO1->FIOMASK &= ~(1 << pin);
}

void led_on(uint8_t port, uint8_t pin){
  LPC_GPIO1->FIOSET |= 1 << pin;
}


void led_off(uint8_t port, uint8_t pin){
  LPC_GPIO1->FIOCLR |= 1 << pin;
}

void led_toggle(uint8_t port, uint8_t pin){
  ((LPC_GPIO1->FIOPIN & (1 << pin)) == 0) ? led_on(port, pin):led_off(port,pin);
}
