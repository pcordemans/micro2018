#ifndef TIMER_H
#define TIMER_H
#include "LPC17xx.h"

/**
  * Initialisation of timer 2
  * Precondition: PCLK = CCLK/4 and CCLK = 96 MHz
  */
void timer2_init();

/**
  * Start the timer and generate interrupt after time ms
  * @param time in ms
  */
void timer2_start(uint32_t time);
#endif
