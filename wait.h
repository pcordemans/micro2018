#ifndef WAIT_H
#define WAIT_H
#include "stdint.h"

void wait(uint32_t ms);
#endif
