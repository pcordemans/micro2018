#include "uart.h"

UART::UART(){
  //Power UART0 enabled upon reset
  //Clocking: CCLK/4 = 24MHz
  LPC_UART0->LCR |= (1<<7); //Set DLAB bit to enable access to the divider registers
  LPC_UART0->FDR = 33; // DIVADDVAL = 1 & MULVAL = 2
  LPC_UART0->DLL = 104;
  LPC_UART0->DLM = 0;
  LPC_UART0->LCR &= ~(1<<7); //Clear DLAB bit to disable access to the divider registers
  LPC_UART0->FCR |= 1; //Enable RX and TX FIFOs
  LPC_PINCON->PINSEL0 |= (5 << 4);       //enable RX0 and TX0
  LPC_PINCON->PINMODE0 = (1 << 7);//Rx0 no pulldown resistor
  LPC_UART0->LCR |= 3; // 8-bit word

}

UART::~UART(){

}

char UART::getc(){
  while((LPC_UART0->LSR & 1) == 0);
  char received = LPC_UART0->RBR;
 return received;
}

void UART::putc(char c){
  LPC_UART0->THR = c;
  while((LPC_UART0->LSR & (1 << 6)) == 0);
}
