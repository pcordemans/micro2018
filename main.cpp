#include "uart.h"

#include"led.h"
#include "wait.h"
#include "timer.h"
#include "assert.h"

extern "C"{
	void TIMER2_IRQHandler(){
		led_on(1,18);
		LPC_TIM2->IR |= 1;
		assert(LPC_TIM2->IR == 0);
	}
}

int main() {
	UART pc;
	led_init(1,18);
	timer2_init();
	timer2_start(500);
	while(true){
		char received = pc.getc();
		pc.putc(received+1);
	}
}
